package main

import(
	"fmt"
	"testing"
	// "time"
)

func BenchmarkInsertionSort(b *testing.B) {
	slc := []int{8, 7, 6, 5, 4, 3, 2, 1, -3, -1, 0}
	for n := 0; n < b.N; n++ {
		InsertionSortFunc(slc)
	}

}

func BenchmarkBuildInSort(b *testing.B) {
	slc := []int{8, 7, 6, 5, 4, 3, 2, 1,-3, -1, 0}
	for n := 0; n < b.N; n++ {
		BuildInSortFunc(slc)
	}
	fmt.Println(slc)
}
