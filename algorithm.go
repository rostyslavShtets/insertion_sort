package main

import(
	// "fmt"
	"sort"
)
// InsertionSortFunc - custom sorting
func InsertionSortFunc(slc []int) {
	for j := 1; j < len(slc); j++ {
		key := slc[j]
	  	i := j - 1
		for i >= 0 && slc[i] > key {
			slc[i+1] = slc[i]
			i = i - 1
		}
	  	slc[i+1] = key
	   }
}

// BuildInSortFunc - build-in sorting
func BuildInSortFunc(slc []int) {
	sort.Ints(slc)
}

// func InsertionSortFunc(slc []int) {
// 	for i := 1; i <= len(slc) - 1; i++ {
// 		j := i
// 		for j > 0 && slc[j] < slc[j-1] {
// 		   slc[j], slc[j-1] = slc[j-1], slc[j]
// 		   j--
// 		}
// 	}
// }